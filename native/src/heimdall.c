/*
 * Heimdall - Highly Effective Intervention Manipulation and Data Analysis
 *            at the Link Layer
 * Copyright (C) 2018 Erick Gonzalez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#define _GNU_SOURCE
#include "huvud.h"
#include "Heimdall/CLogging_stub.h"

huvud_queue_t* queue_init(uint16_t q_id,
                          char* component,
                          const char* device,
                          const char* filter_exp)
{
  huvud_queue_t* hq_p = huvud_init(q_id, component, device, filter_exp, &cLog);

  if (hq_p) {
    hq_p->analyze = &cAnalyze;
  }

  return hq_p;
}

void
run_queue(HsPtr q_p, huvud_queue_t* hq_p) {
  hq_p->q_p = q_p;
  huvud_run_queue(hq_p);
}

void queue_destroy(huvud_queue_t* hq_p) {
  huvud_destroy(hq_p);
}

packet_t* allocate_pkt() {
  return malloc(sizeof(packet_t));
}

void free_pkt(packet_t* pkt) {
  free(pkt);
}
