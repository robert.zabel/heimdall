/*
 * pcap.c - Heimdall default interface using libPCAP
 * Copyright (C) 2018 Erick Gonzalez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#define _GNU_SOURCE
#include <errno.h>
#include <sys/time.h>
#include "huvud.h"
#include "l3.h"

static const int MAX_CAPTURE_SIZE = 8192;

static const char* datalink_name(int link_type) {
  switch (link_type) {
    case DLT_NULL:
        return "null";
    case DLT_EN10MB:
        return "ethernet_10Mb";
    case DLT_EN3MB:
        return "experimental_ethernet_3Mb";
    case DLT_AX25:
        return "ax25";
    case DLT_ARCNET:
        return "arcnet";
    case DLT_SLIP:
        return "slip";
    case DLT_PPP:
        return "ppp";
    case DLT_FDDI:
        return "fddi";
    case DLT_PPP_SERIAL:
        return "ppp_serial";
    case DLT_PPP_ETHER:
        return "ppp_ether";
    case DLT_ATM_RFC1483:
        return "atm_rfc1483";
    case DLT_RAW:
        return "raw";
    case DLT_C_HDLC:
        return "c_hdlc";
    case DLT_IEEE802_11:
        return "ieee802_11";
    case DLT_FRELAY:
        return "frelay";
    case DLT_LOOP:
        return "loop";
    case DLT_LINUX_SLL:
        return "linux_sll";
    case DLT_LTALK:
        return "ltalk";
    case DLT_PFLOG:
        return "pflog";
    case DLT_PRISM_HEADER:
        return "prism_header";
    case DLT_IP_OVER_FC:
        return "ip_over_fc";
    case DLT_SUNATM:
        return "sunatm";
    case DLT_IEEE802_11_RADIO:
        return "ieee802_11_radio";
    case DLT_ARCNET_LINUX:
        return "arcnet_linux";
    case DLT_APPLE_IP_OVER_IEEE1394:
        return "apple_ip_over_ieee1394";
    case DLT_MTP2_WITH_PHDR:
        return "mtp2_with_phdr";
    case DLT_MTP2:
        return "mtp2";
    case DLT_MTP3:
        return "mtp3";
    case DLT_SCCP:
        return "sccp";
    case DLT_DOCSIS:
        return "docsis";
    case DLT_LINUX_IRDA:
        return "linux_irda";
    case DLT_USER15:
        return "user15";
    case DLT_IEEE802_11_RADIO_AVS:
        return "ieee802_11_radio_avs";
    case DLT_BACNET_MS_TP:
        return "bacnet_ms_tp";
    case DLT_PPP_PPPD:
        return "ppp_pppd";
    case DLT_GPRS_LLC:
        return "gprs_llc";
    case DLT_GPF_T:
        return "gpf_t";
    case DLT_GPF_F:
        return "gpf_f";
    case DLT_LINUX_LAPD:
        return "linux_lapd";
    case DLT_BLUETOOTH_HCI_H4:
        return "bluetooth_hci_h4";
    case DLT_USB_LINUX:
        return "usb_linux";
    case DLT_PPI:
        return "ppi";
    case DLT_SITA:
        return "sita";
    case DLT_ERF:
        return "erf";
    case DLT_BLUETOOTH_HCI_H4_WITH_PHDR:
        return "bluetooth_hci_h4_with_phdr";
    case DLT_AX25_KISS:
        return "ax25_kiss";
    case DLT_LAPD:
        return "lapd";
    case DLT_PPP_WITH_DIR:
        return "ppp_with_dir";
    case DLT_C_HDLC_WITH_DIR:
        return "c_hdlc_with_dir";
    case DLT_FRELAY_WITH_DIR:
        return "frelay_with_dir";
    case DLT_IPMB_LINUX:
        return "ipmb_linux";
    case DLT_IEEE802_15_4_NONASK_PHY:
        return "ieee802_15_4_nonask_phy";
    case DLT_USB_LINUX_MMAPPED:
        return "usb_linux_mmapped";
    case DLT_FC_2:
        return "fc_2";
    case DLT_FC_2_WITH_FRAME_DELIMS:
        return "fc_2_with_frame_delims";
    case DLT_IPNET:
        return "ipnet";
    case DLT_CAN_SOCKETCAN:
        return "can_socketcan";
    case DLT_IPV4:
        return "ipv4";
    case DLT_IPV6:
        return "ipv6";
    case DLT_IEEE802_15_4_NOFCS:
        return "ieee802_15_4_nofcs";
    case DLT_DBUS:
        return "dbus";
    case DLT_DVB_CI:
        return "dvb_ci";
    case DLT_MUX27010:
        return "mux27010";
    case DLT_STANAG_5066_D_PDU:
        return "stanag_5066_d_pdu";
    case DLT_NFLOG:
        return "nflog";
    case DLT_NETANALYZER:
        return "netanalyzer";
    case DLT_NETANALYZER_TRANSPARENT:
        return "netanalyzer_transparent";
    case DLT_IPOIB:
        return "ipoib";
    case DLT_MPEG_2_TS:
        return "mpeg_2_ts";
    case DLT_NG40:
        return "ng40";
    case DLT_NFC_LLCP:
        return "nfc_llcp";
    case DLT_INFINIBAND:
        return "infiniband";
    case DLT_SCTP:
        return "sctp";
    case DLT_USBPCAP:
        return "usbpcap";
    case DLT_RTAC_SERIAL:
        return "rtac_serial";
    case DLT_BLUETOOTH_LE_LL:
        return "bluetooth_le_ll";
    case DLT_NETLINK:
        return "netlink";
    case DLT_BLUETOOTH_LINUX_MONITOR:
        return "bluetooth_linux_monitor";
    case DLT_BLUETOOTH_BREDR_BB:
        return "bluetooth_bredr_bb";
    case DLT_BLUETOOTH_LE_LL_WITH_PHDR:
        return "bluetooth_le_ll_with_phdr";
    case DLT_PROFIBUS_DL:
        return "profibus_dl";
    case DLT_PKTAP:
        return "pktap";
    case DLT_EPON:
        return "epon";
    case DLT_IPMI_HPM_2:
        return "ipmi_hpm_2";
    case DLT_ZWAVE_R1_R2:
        return "zwave_r1_r2";
    case DLT_ZWAVE_R3:
        return "zwave_r3";
    case DLT_WATTSTOPPER_DLM:
        return "wattstopper_dlm";
    case DLT_ISO_14443:
        return "iso_14443";
    case DLT_RDS:
        return "rds";
  }
  return "<unknown link type>";
}

u_char l2_offset(int link_type) {
  switch (link_type) {
  case DLT_EN10MB:
    return 14; // Ethernet frame header
  default:
    return 0;
  }
}

void pcap_handle_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
  huvud_queue_t* hq_p = (typeof (hq_p)) args;
  bpf_u_int32 len = header->caplen;
  bpf_u_int32 ts  = header->ts.tv_sec * 1000000 + header->ts.tv_usec;

  u_char offset = l2_offset(hq_p->hw.link_type);

#ifdef __DEBUG__
  char buf[4096];

  hexdump(buf, sizeof(buf), packet, min(64, len));
  LOG(hq_p, DEBUG, "%u length %d\n%s", ts, len, buf);
#endif

  switch(handle_l3_packet(hq_p, ts, (u_char*) packet + offset, len)) {
  case ABORT_ALL_PROCESSING:
    LOG(hq_p, INFO, "Aborting PCAP processing instruction on queue #%d", hq_p->id);
    pcap_breakloop(hq_p->hw.session);
    break;
  default:
    break;
  }
}

int hw_init(huvud_queue_t* hq_p, const char* device, const char* filter_exp) {
  char errbuf[PCAP_ERRBUF_SIZE];
  int rc = OK;
  bpf_u_int32 mask, net;

  LOG(hq_p, INFO, "Initializing PCAP on queue #%d", hq_p->id);

  if (!device || !*device) {
    device = pcap_lookupdev(errbuf);
    if (!device) {
      LOG(hq_p, ERROR, "Unable to lookup default device: %s", errbuf);
      goto out;
    }
  }

  if (pcap_lookupnet(device, &net, &mask, errbuf) == -1) {
    LOG(hq_p, ERROR, "Failed to read device %s network address: %s", device, errbuf);
    rc = EINVAL;
    goto out;
  }

  hq_p->hw.session = pcap_open_live(device, MAX_CAPTURE_SIZE, 0, 100 /* ms */, errbuf);

  if (!hq_p->hw.session) {
    LOG(hq_p, ERROR, "Failed to open device %s for capture: %s", device, errbuf);
    rc = EPERM;
    goto out;
  }

  hq_p->hw.link_type = pcap_datalink(hq_p->hw.session);

  LOG(hq_p, INFO, "Opened %s (type %u) device %s on queue %d",
      datalink_name(hq_p->hw.link_type),
      hq_p->hw.link_type, device, hq_p->id);

  if (filter_exp && *filter_exp) {
    if (pcap_compile(hq_p->hw.session, &hq_p->hw.fp, filter_exp, 1, mask) == -1) {
      LOG(hq_p, ERROR, "Error parsing filter expresion \"%s\" for device %s: %s",
          filter_exp, device, pcap_geterr(hq_p->hw.session));
      goto err;
    }
    if (pcap_setfilter(hq_p->hw.session, &hq_p->hw.fp) == -1) {
      LOG(hq_p, ERROR, "Failed to set filter \"%s\" on %s: %s", filter_exp, device,
          pcap_geterr(hq_p->hw.session));
      goto err;
    }
    LOG(hq_p, INFO, "Set filter \"%s\" on %s successfuly", filter_exp, device);
  }
 out:
  return rc;
 err:
  hw_destroy(hq_p);
  goto out;
}

void hw_run_queue(huvud_queue_t* hq_p) {
  LOG(hq_p, INFO, "PCAP Sniffing packets for queue #%d", hq_p->id);
  pcap_loop(hq_p->hw.session, -1, pcap_handle_packet, (u_char*) hq_p);
  LOG(hq_p, INFO, "Stopped sniffing PCAP packets for queue #%d", hq_p->id);
}

void hw_destroy(huvud_queue_t* hq_p) {
  if (hq_p) {
    pcap_close(hq_p->hw.session);
    hq_p->hw.session = NULL;
  }
}
