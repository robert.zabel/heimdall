/*
 * Huvud - Heimdall Universal Virtual Uniform Data-Interface
 * Copyright (C) 2018 Erick Gonzalez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include "huvud.h"

void hexdump(char* buf, uint32_t bufLen, const void* data, uint32_t len) {
  const uint8_t* ptr = data;
  size_t   col = 0;

  for (size_t off = 0; off <= len; off += 16) {
    SNPRINTF(buf, bufLen, "%04lX: ", off);

    for (size_t b = off; b < off + 16; b += 8) {
      for (size_t x = b; x < b + 8; ++ x)  {
        SNPRINTF(buf, bufLen, "%02X ", *(ptr+x));
      }
      SNPRINTF(buf, bufLen, "   ");
    }
    for (size_t x = off; x < off + 16; ++x) {
      uint8_t c = *(ptr+x);
      if (c >= 32 && c <= 127) {
        SNPRINTF(buf, bufLen, "%c", c);
      } else {
        SNPRINTF(buf, bufLen, ".");
      }
    }
    SNPRINTF(buf, bufLen, "\n");
  }
}

huvud_queue_t*
huvud_init(uint16_t q_id,
           char* component,
           const char* device,
           const char* filter_exp,
           logfn_t* log_fn) {
  huvud_queue_t* hq_p = malloc(sizeof(*hq_p));

  if (!hq_p) {
    LOG2(log_fn, ERROR, component, "Failed to allocate huvud queue %d", q_id);
    goto out;
  }

  hq_p->id  = q_id;
  strncpy(hq_p->component, component, sizeof(hq_p->component));
  hq_p->log = log_fn;

  if (hw_init(hq_p, device, filter_exp) != OK) {
    goto err1;
  };

  LOG(hq_p, DEBUG, "Allocated queue %d @0x%p", q_id, hq_p);

  goto out;

 err1:
  free(hq_p);
  hq_p = NULL;

 out:
  return hq_p;
}

void huvud_run_queue(huvud_queue_t* hq_p) {
  hw_run_queue(hq_p);
}

void huvud_destroy(huvud_queue_t* hq_p) {
  hw_destroy(hq_p);
  free(hq_p);
}
