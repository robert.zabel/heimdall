/*
 * Heimdall L4 specific handling
 *
 * Copyright (C) 2018 Erick Gonzalez
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#define _GNU_SOURCE
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>

#include "huvud.h"

void parse_udp(packet_t* pkt, struct udphdr* udp, uint32_t len) {
  if (len >= sizeof(*udp)) {
    pkt->protocol = IPPROTO_UDP; /* we do this here so that this is only set if we do
                                  * see the complete UDP header
                                  */
    pkt->src_port          = ntohs(udp->uh_sport);
    pkt->dst_port          = ntohs(udp->uh_dport);
    pkt->total_payload     = ntohs(udp->uh_ulen) - sizeof(*udp);
    pkt->available_payload = len - sizeof(*udp);
    pkt->payload           = ((uint8_t*)udp) + sizeof(*udp);
  }
}

void parse_tcp(packet_t* pkt, struct tcphdr* tcp, uint32_t len) {
  if (len >= sizeof(*tcp)) {
    pkt->protocol      = IPPROTO_TCP; /* set this only if we do get a complete TCP header */
    pkt->src_port      = ntohs(tcp->th_sport);
    pkt->dst_port      = ntohs(tcp->th_dport);
    pkt->total_payload = 0 ; /* there is no way to know ahead how much data will be sent in a
                                TCP connection */
    uint32_t header_len    = tcp->th_off * 4;
    if (len > header_len) {
      pkt->available_payload = len - header_len;
      pkt->payload           = ((uint8_t*)tcp) + header_len;
    } else {
      pkt->payload           = NULL;
      pkt->available_payload = 0;
    }
  }
}

void parse_l4(packet_t* pkt, uint8_t proto, uint8_t* l4_data, uint32_t len) {
  while (len > 0) {
    switch(proto) {
      case IPPROTO_UDP: {
        struct udphdr* udp = (struct udphdr*) l4_data;
        parse_udp(pkt, udp, len);
        len = 0; /* stop after parsing UDP header */
        break;
      }
      case IPPROTO_TCP: {
        struct tcphdr* tcp = (struct tcphdr*) l4_data;
        parse_tcp(pkt, tcp, len);
        len = 0; /* stop after parsing TCP header */
        break;
      }
      case IPPROTO_FRAGMENT:
      case IPPROTO_HOPOPTS:
      case IPPROTO_DSTOPTS:
      case IPPROTO_ROUTING: {
        if (len < 2) {
          len = 0; /* not enough data */
          break;
        }

        uint8_t next_hdr = *l4_data;
        uint8_t hdr_len  = proto == IPPROTO_FRAGMENT ? 8 : (*(l4_data+1) + 1) * 8;

        if (hdr_len == 0) {
          len = 0;
          break;
        }
        l4_data += hdr_len;
        if (len <= hdr_len) {
          len = 0;
          break; /* not enough data */
        }
        len -= hdr_len;
        parse_l4(pkt, next_hdr, l4_data, len);
        break;
      }
      default: {
        len = 0;
        break;
      }
    }
  }
}
