{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Hofund - Heimdall CLI interface commands
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}

module Hofund.Commands.Interface where

import Control.Monad                 (void)
import Control.Monad.Except          (runExceptT)
import Control.Monad.Failable        (failableIO)
import Control.Monad.IO.Class        (liftIO)
import Control.Monad.Trans           (lift)
import Control.Monad.Trans.Maybe     (MaybeT(..), runMaybeT)
import Control.Monad.State.Strict    (gets, modify)
import Data.ByteString.Char8         (ByteString, pack, unpack)
import Data.Configurable             (Configurable(..), Serializations, serializations)
import Data.Monoid                   ((<>))
import Database.Adapter              (Adapter(..), del)
import Heimdall.Common.Encapsulation (Encapsulation)
import Heimdall.Types                hiding (Action)
import Hofund.Commands.Common
import Hofund.Types
import System.Console.StructuredCLI  hiding (Commands)

intfKey :: IntfName -> ByteString
intfKey (IntfName (intfType, iD)) =
    intfKeyPrefix <> (pack $ show intfType) <> "." <> pack (show iD)

interface :: Commands
interface =
  custom "interface"  "<interface name to configure>" parseIntf always checkIntf >+ do
              command "add" "Add a configuration element to this interface" newLevel >+ do
                basic
                addPolicy
              command "remove" "Remove a configuration element from this interface" newLevel >+ do
                basic
                delACL
                delSrvAddress
                delPolicy
                delNode
                delDevice
                delType
                delConnTag
                delMasquerading
              command "set" "Set a given property for this interface" newLevel >+ do
                basic
                setACL
                setSrvAddress
                setNode
                setDevice
                setType
                setConnTag
                setMasquerading
              basic
              enable
              encap
              disable
              destroy
              queueType
              showIntf
              vrfId

checkIntf :: IntfName -> StateM Action
checkIntf intf = do
  let intfType = getIntfType intf
  modify $ \state -> state { inTunnel = intfType == TunnelIntf,
                             inQueue  = intfType == QueueIntf,
                             inVIF    = intfType == VIFIntf,
                             target   = intfKey intf }
  return NewLevel

whenQueue :: StateM Bool
whenQueue = gets inQueue

whenTunnel :: StateM Bool
whenTunnel = gets inTunnel

addPolicy :: Commands
addPolicy =
  custom "policy" hint (parseNameFor policyKeyPrefix hint) always setArg1 >+ do
    custom  "matching" hint' (parseNameFor aclKeyPrefix hint') always $
      \acl -> do
        intfKeyName <- getTarget
        policyName  <- getArg1
        let key = intfKeyName <> ".policies"
        db <- gets db
        setMapKVs db key [(pack acl, policyName)]
        return NoAction
          where hint  = "<routing policy to add to this interface>"
                hint' = "<ACL which should match to select this policy>"

delPolicy :: Commands
delPolicy =
  custom "policy" hint (parseNameFor policyKeyPrefix hint) always setArg1 >+ do
    custom  "matching" hint' (parseNameFor aclKeyPrefix hint') always $
      \acl -> do
        intfKeyName <- getTarget
        let key = intfKeyName <> ".policies"
            aclName = pack acl
        db         <- gets db
        policyName <- getArg1
        currentPolicyName <- getMapKV db key aclName
        if currentPolicyName == policyName
          then delMapKVs db key [aclName]
          else liftIO . putStrLn $ "Failed to remove policy " ++ unpack policyName
                   ++ " matching " ++ acl ++ ": current policy is " ++ unpack currentPolicyName
        return NoAction
          where hint = "<routing policy to remove from this interface>"
                hint' = "<ACL which should match to select policy being removed>"
setSrvAddress :: Commands
setSrvAddress =
  setIntfProperty "service-address" "Set interface service IP address" parseIP whenTunnel

delSrvAddress :: Commands
delSrvAddress =
  command' "service-address" "Remove interface service address" whenTunnel $ delProperty "service-address"

setACL :: Commands
setACL =
  custom "acl" hint (parseNameFor aclKeyPrefix hint) whenQueue $
    \acl -> do
      db <- gets db
      intfKeyName <- getTarget
      failableIO $ setMapKVs db intfKeyName [("acl", pack acl)]
      return NoAction
          where hint = "<ACL to use as traffic selector for this interface>"

delProperty :: ByteString -> StateM Action
delProperty property = do
  db <- gets db
  intfKeyName <- getTarget
  failableIO $ delMapKVs db intfKeyName [property]
  return NoAction

delACL :: Commands
delACL =
  command' "acl" hint whenQueue $ delProperty "acl"
        where hint = "<ACL to remove from this interface>"

setMasquerading :: Commands
setMasquerading =
  command "masquerading" "Activate masquerading on this interface" $ do
    isVIF <- gets inVIF
    if not isVIF
      then liftIO $ putStrLn "Masquerading is currently only supported on virtual interfaces (VIFs)"
      else do
        intfKeyName <- getTarget
        setInterface intfKeyName "masquerading" True
    return NoAction

delMasquerading :: Commands
delMasquerading =
  command "masquerading" "Deactivate masquerading on this interface" $
    delProperty "masquerading"

setIntfProperty :: (Configurable a) => String
                                    -> String
                                    -> Parser StateM a
                                    -> StateM Bool
                                    -> Commands
setIntfProperty property hint parser isEnabled =
  custom property hint parser isEnabled $ \value -> do
    intfKeyName <- getTarget
    setInterface intfKeyName (pack property) value
    return NoAction

setDevice :: Commands
setDevice =
  setIntfProperty "device" hint (parseName hint) always
      where hint = "<network device associated with this interface>"

setType :: Commands
setType =
  setIntfProperty "type" hint (parseName hint) always
      where hint = "<type of network device if configured>"

encap :: Commands
encap =
  setIntfProperty "encapsulation" hint parseEncap whenTunnel
      where parseEncap node input = do
              keywords :: Serializations Encapsulation <- serializations
              let encaps = unpack . snd <$> keywords
              parseOneOf encaps hint node input
            hint = "<tunnel encapsulation type>"

enable :: Commands
enable =
  command "enable" "Enable interface operation" $ do
    setEnabled True
    return NoAction

disable :: Commands
disable =
  command "disable" "Shutdown interface" $ do
    setEnabled False
    return NoAction

setNode :: Commands
setNode =
  setIntfProperty "node" hint (parseName hint) always
    where hint = "<node on which this interface is localized>"

delNode ::Commands
delNode =
  command "node" "Remove interface from given node" $ delProperty "node"

delDevice ::Commands
delDevice =
  command "device" "<remove association to this network device>" $ delProperty "device"

delType ::Commands
delType =
  command "type" "<remove network device type>" $ delProperty "type"

setConnTag :: Commands
setConnTag =
  setIntfProperty "connection-tag" "<apply this tag to connections over this interface" parseConnTag always

delConnTag :: Commands
delConnTag =
  command "connection-tag" "<remove tagging of connections over this interface" $
    delProperty "connection-tag"

vrfId :: Commands
vrfId =
  setIntfProperty "vrf-id" "<vrf-id to assign interface to>" parseVRFID always

queueType :: Commands
queueType =
  setIntfProperty "service-type" hint parseServiceType whenQueue
      where parseServiceType node input = do
              keywords :: Serializations ServiceType <- serializations
              let serviceTypes = unpack . snd <$> keywords
              parseOneOf serviceTypes hint node input
            hint             = "<queue type of service>"

setEnabled :: Bool -> StateM ()
setEnabled isEnabled = void . runMaybeT $ do
  intfKeyName <- lift getTarget
  lift $ setInterface intfKeyName "enabled" isEnabled

setInterface :: (Configurable a) => ByteString -> ByteString -> a -> StateM ()
setInterface intfKeyName property value = do
  status <- runExceptT $ do
             str <- serialize value
             db  <- lift $ gets db
             failableIO $ setMapKVs db intfKeyName [(property, str)]
  either (cmdFailure $ "setting " ++ unpack intfKeyName ++ ' ':unpack property) return status

showIntfs :: Commands
showIntfs =
  command "interfaces" "List all configured interfaces" $ do
    intfs <- getAllIntfs
    printOutput $ mapM_ print intfs
    return NoAction

showOne :: ByteString -> StateM ()
showOne key = do
  let policies = key <> ".policies"
  showObject key Nothing
  liftIO $ putStrLn "policies matching:"
  showObject policies Nothing

showIntf :: Commands
showIntf =
  command "show" "Show interface configuration" $ do
    intfKeyName <- getTarget
    showOne intfKeyName
    return NoAction

destroy :: Commands
destroy =
  command "destroy" "Remove this interface from the system" $ do
    db <- gets db
    intfKeyName <- getTarget
    del db [intfKeyName]
    return NoAction
