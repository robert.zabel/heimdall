{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Object Model utility functions
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.ObjectModel where

import Prelude             hiding (span)
import Control.Monad.Except       (runExceptT)
import Control.Monad.Fix          (fix)
import Control.Monad.IO.Class     (MonadIO, liftIO)
import Control.Concurrent         (threadDelay)
import Control.Exception          (SomeException, catch, throw)
import Control.Monad.Failable     (Failable(..))
import Data.Monoid                ((<>))
import Data.ByteString.Char8      (ByteString, append, span, stripPrefix)
import Data.Configurable          (Configurable, deserialize)
import Data.List                  (any)
import Data.Maybe                 (catMaybes)
import Data.Typeable              (Typeable)
import Database.Adapter           (Event(..),
                                   SubAction(..),
                                   get,
                                   searchAll,
                                   uponChange)
import Heimdall.Environment
import Heimdall.Exceptions
import System.Logging

import qualified Control.Exception as CE

data ObjectModelError = DeserializationFailure String
                      deriving (Typeable, Show)

instance CE.Exception ObjectModelError

monitor :: (?env::Env) => String -> [ByteString] -> (ByteString -> IO()) -> (ByteString -> IO ()) -> IO ()
monitor component prefixes changeFn deleteFn =
    fix . (>>) $ monitor' `catch` \(e::SomeException) -> do
      warningM component $ "Monitoring failure: " ++ show e
      threadDelay 3000000
        where db             = getDB ?env
              reconfigure ev = reconfigure' ev >> return Continue
              reconfigure' (Changed _ key) = changeFn key
              reconfigure' (Added   _ key) = changeFn key
              reconfigure' (Deleted _ key) = deleteFn key
              reconfigure' ev              =
                  throw . InternalError $ "Unexpected event: " ++ show ev
              wildcards = (`append` "*") <$> prefixes
              monitor'  = do
                keys <- noDupKeys prefixes . concat <$> mapM (searchAll db) wildcards
                liftIO $ mapM_ changeFn keys
                uponChange db wildcards reconfigure

noDupKeys :: [ByteString] -> [ByteString] -> [ByteString]
noDupKeys prefixes = filter (not . isSubKey)
    where isSubKey key   = any hasSubProperty . catMaybes $ fmap (flip stripPrefix $ key) prefixes
          hasSubProperty = (/= "") . snd . span (/= '.')

deserializeVal :: (?env::Env, Configurable a, MonadIO m, Failable m)
                  => ByteString
                  -> ByteString
                  -> m a
deserializeVal prefix name = do
  result <- runExceptT . get db $ prefix <> name
  flip (either failed) result $ \str -> do
    eVal <- runExceptT $ deserialize str
    either failed return eVal
          where db         = getDB ?env
                failed err = failure $ DeserializationFailure $ show err
