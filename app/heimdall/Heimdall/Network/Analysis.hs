--{-# LANGUAGE OverloadedStrings #-}

module Heimdall.Network.Analysis where

--import Control.Lens                    ((&), (^.), (.~), (?~))
import Control.Monad.IO.Class          (MonadIO, liftIO)
-- import Control.Monad.State.Strict      (evalStateT)
-- import Control.Applicative             ((<|>))
import Data.ByteString.Unsafe          (unsafePackCStringLen)
import Control.Monad.Failable          (Failable)
import Heimdall.Connections
-- import Heimdall.Network.Analysis.HTTP  (parseHTTP)
-- import Heimdall.Network.Analysis.TLS   (parseTLS)
import Heimdall.Packet
import Heimdall.Types
--import System.Logging

-- import qualified Data.Attoparsec.ByteString.Char8 as A

analyze :: (Failable m, MonadIO m) => Connection -> Filter -> Packet -> m (Status, Connection)
analyze _conn _filtr _pkt@Packet {..} =
  liftIO $ unsafePackCStringLen (getPayload, fromIntegral getAvailablePayload) >>=
    undefined
--    analyze' . parse
--    where state0 = AnalState { _analFilter = filtr,
--                               _analPacket = pkt }
--          parse bytes = case conn ^. analysis . continue of
--                          Nothing ->
--                            evalStateT parseL57 state0 `A.parse` bytes
--                          Just continuation ->
--                            continuation bytes
--          analyze' (A.Done _ result') =
--            return (Finished, conn & analysis .~ result')
--          analyze' (A.Partial continuation') =
--            return (Incomplete, conn & analysis . continue ?~ continuation')
--          analyze' (A.Fail _ ctx msg) = do
--            debugM "Network.Analysis" $ "Parse failure: " ++ show msg ++ ": " ++ show ctx
--            return (Failed, conn & analysis . continue .~ Nothing)

-- parseL57 :: AnalMonad L57Analysis
-- parseL57 = parseHTTP <|> parseTLS
