module Heimdall.Network.Analysis.HTTP where

import Control.Lens                     (use)
import Heimdall.Types

parseHTTP :: AnalMonad L57Analysis
parseHTTP = do
    analDepth <- use $ analFilter . filterLayer
    case analDepth of
        InDepth ->
            undefined
        _ ->
            undefined