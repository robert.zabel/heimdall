{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-unused-imports #-}

module Heimdall.Network.Foreign.Utils where

import Control.Exception       (Exception)
import Control.Monad           (when)
import Control.Monad.Failable  (Failable(..), failableIO)
import Control.Monad.IO.Class  (MonadIO)
import Data.Int                (Int32)
import Data.Typeable           (Typeable)
import Foreign.C
import Foreign.Ptr             (Ptr)
import Foreign.Marshal.Alloc   (alloca)
import Foreign.Storable        (poke, sizeOf)
import Network.Socket

data NetworkError = NetworkError String deriving (Typeable, Show)

instance Exception NetworkError

#include <sys/socket.h>
#include <netinet/in.h>

#ifdef LINUX
#include <linux/ip.h>
#endif

solSocket :: CInt
solSocket = #const SOL_SOCKET

ipProtoIP :: CInt
ipProtoIP = #const IPPROTO_IP

#ifdef LINUX

foreign import ccall "setsockopt" setsockopt_cstr :: CInt ->
                                                     CInt ->
                                                     CInt ->
                                                     CString ->
                                                     CInt ->
                                                     IO CInt

foreign import ccall "setsockopt" setsockopt :: CInt ->
                                                CInt ->
                                                CInt ->
                                                Ptr a ->
                                                CInt ->
                                                IO CInt

foreign import ccall "strerror"   strerror        :: CInt -> IO CString

soBindToDevice :: CInt
soBindToDevice = #const SO_BINDTODEVICE

bindToDevice :: (MonadIO m, Failable m) => Socket -> String -> m ()
bindToDevice sock device =
    failableIO $ withCString device $ \optval -> do
      let devLength = fromIntegral $ length device
      retVal <- setsockopt fd solSocket soBindToDevice optval devLength
      when (retVal /= 0) $ do
        Errno errno <- getErrno
        cError <- strerror errno
        msg    <- peekCString cError
        failure . NetworkError $ "bindToDevice " ++ device ++ ": " ++ msg
          where fd = fdSocket sock

soMark :: CInt
soMark = #const SO_MARK

setConnectionTag :: (MonadIO m, Failable m) => Socket -> Int32 -> m ()
setConnectionTag sock mark = failableIO . alloca $ \ptr -> do
  let mark' = fromIntegral mark :: CInt
  poke ptr mark'
  retVal  <- setsockopt fd solSocket soMark ptr $ fromIntegral (sizeOf mark')
  when (retVal /= 0) $
    failure . NetworkError $ "Failed to set connection tag as mark: " ++ show mark
          where fd = fdSocket sock

ipHdrIncl :: CInt
ipHdrIncl = #const IP_HDRINCL

setHdrIncl :: (MonadIO m, Failable m) => Socket -> m ()
setHdrIncl sock = failableIO . alloca $ \ptr -> do
  let yes = 1 :: CInt
  poke ptr yes
  retVal  <- setsockopt fd ipProtoIP ipHdrIncl ptr $ fromIntegral (sizeOf yes)
  when (retVal /= 0) $
    failure . NetworkError $ "Failed to set IP_HDRINCL option"
          where fd = fdSocket sock

#else

bindToDevice :: (MonadIO m, Failable m) => Socket -> String -> m ()
bindToDevice _ _ = return ()

setConnectionTag :: (MonadIO m, Failable m) => Socket -> Int32 -> m ()
setConnectionTag _ _ = return ()

setHdrIncl :: (MonadIO m, Failable m) => Socket -> m ()
setHdrIncl _ = return ()

#endif

