{-# LANGUAGE CPP #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{- |
Description: Heimdall Routing
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}

module Heimdall.Routing (Result(..), handleRxPacket, routePacket) where

import Control.Lens                 ((^.), (.=), makeLenses, set, use)
import Control.Monad                (mapM_, when)
import Control.Monad.Except         (runExceptT)
import Control.Monad.Failable       (Failable(..))
import Control.Monad.IO.Class       (MonadIO, liftIO)
import Control.Monad.State.Strict   (StateT, execStateT, get)
import Control.Monad.Trans          (lift)
import Control.Monad.Trans.Maybe    (MaybeT(..), runMaybeT)
import Control.Applicative          ((<|>))
import Data.ByteString.Char8        (ByteString, unpack)
import Data.Default                 (def)
import Data.Hashable                (hash)
import Data.IP                      (IP(..), makeAddrRange)
import Data.List                    (find)
import Data.Maybe                   (isNothing, fromMaybe)
import Data.Referable               (deRef, getRefName)
import Data.Vector                  ((!))
import Heimdall.Connections
import Heimdall.Connection.Tracking (updateConnection)
import Heimdall.Exceptions
import Heimdall.Foreign.Packet      (withUnsafeL3Packet)
import Heimdall.Injector            (inject)
import Heimdall.Interfaces
import Heimdall.Network.Analysis    (analyze)
import Heimdall.Network.Filter      (evalFilter)
import Heimdall.Network.Flow        (Flow, Direction(..), flowFor)
import Heimdall.Packet              (Packet(..), Protocol(..))
import Heimdall.Types
import System.Logging

import qualified Data.IP.RouteTable        as RT
import qualified Data.Vector               as V

data State = State { _interimResult :: Result,
                     _inputIntf     :: Interface,
                     _packet        :: Packet,
                     _connection    :: Maybe Connection,
                     _packetFlow    :: Maybe Flow,
                     _direction     :: Maybe Direction }

makeLenses ''State

type RoutingT = StateT State

setResult :: (MonadIO m) => Result -> RoutingT m ()
setResult result = do
  previous <- use interimResult
  when (isOverridable previous) $ do
    debugM "Routing.Processing" $ "Routing result is now " ++ show result
    interimResult .= result

routePacket :: IDB -> Packet -> IO Result
routePacket idb pkt = do
  result <- runMaybeT $ do
              intf <- getIntfM idb
              routePacket_ intf pkt
  maybe (failed "missing or disabled ingress interface") return result

routePacket_ :: (Failable m, MonadIO m) => Interface -> Packet -> m Result
routePacket_ intf@Interface{..} pkt = do
  result <- runExceptT $ do
              let state0 = State { _interimResult = def,
                                   _inputIntf     = intf,
                                   _packet        = pkt,
                                   _connection    = Nothing,
                                   _packetFlow    = Nothing,
                                   _direction     = Nothing }
              State{..} <- withState state0 routePacket'
              return _interimResult
  result' <- either (failed . show) return result
  debugM "Routing.Outgoing" $ show result' ++ ' ':show pkt
  return result'
    where routePacket'
              | intf <?> can RxData = do
                  debugM "Routing.Incoming" $ show _intfName ++ " <- " ++ show pkt
                  mapM_ applyPolicies _intfPolicyMap
              | otherwise = do
                  debugM "Routing.Incoming" $
                             "Discarding unexpected packet on transmit only interface: "
                             ++ show pkt
                  setResult Drop
          withState = flip execStateT

failed :: (MonadIO m) => String -> m Result
failed msg = do
  infoM "Routing" $ "Routing failure due to " ++ msg
  return Abort

applyPolicies :: (MonadIO m, Failable m) => (ACLRef, PolicyRef) -> RoutingT m ()
applyPolicies (aclRef, policyRef) = do
    result <- use interimResult
    when (isOverridable result) $ do
      access <- applyACL aclRef
      when (access == Permit) $ deRef policyRef >>= applyPolicy
        where applyPolicy (Just Policy{..}) = mapM_ applyRules policyRules
              applyPolicy Nothing           = do
                let policyName = unpack $ getRefName policyRef
                debugM "Routing.Processing" $ "Policy " ++ policyName ++ " not configured"

isOverridable :: Result -> Bool
isOverridable Bypass = True
isOverridable Routed = True
isOverridable None   = True
isOverridable _      = False

applyACL :: (MonadIO m, Failable m) => ACLRef -> RoutingT m Access
applyACL ref = do
  pkt    <- use packet
  inIntf <- use inputIntf
  result <- runMaybeT $ do
    ACL{..} <- MaybeT $ deRef ref
    debugM "Routing.Processing" $ "Applying ACL " ++ unpack aclName
    flow <- lift getPacketFlow
    let srcIP = getSrcIP pkt
        vrfID = inIntf ^. intfVRFId
    conn@Connection {..} <- updateConnection vrfID flow $ set source (Just srcIP)
    connection .= Just conn
    dir <- lift $ getDirection conn

    if dir == Downstream
       then return Permit
       else do
         (_, ACE{..}) <- MaybeT . pure $ find (matchingACE inIntf pkt) aclRules
         return _aceAccess

  debugM "Routing.Processing" $ "ACL result is " ++ show result
  return $ fromMaybe Deny result

matchingACE :: Interface -> Packet -> ACLRule -> Bool
matchingACE Interface{..} Packet{..} (_, ACE{..}) =
  let result = (_aceSrcRange >>= nothingIfInRange getSrcIP) <|>
               (_aceDstRange >>= nothingIfInRange getDstIP) <|>
               (_aceProtocol >>= nothingIf protocolMatches getProto) <|>
               (_acePorts    >>= nothingIf portInRange (fromIntegral getDstPort)) <|>
               (_aceDevice   >>= nothingIf (==) (show _intfName))
      protocolMatches _ IP_Protocol = True
      protocolMatches a b = a == b
  in fromMaybe True result
      where nothingIfInRange ip@IPv4{} (IPRange range@(IPv4{},IPv4{})) = nothingIf inRange ip range
            nothingIfInRange ip@IPv6{} (IPRange range@(IPv6{},IPv6{})) = nothingIf inRange ip range
            nothingIfInRange _                  _                      = Just False
            inRange ip (rangeStart,rangeEnd) = rangeStart <= ip && ip <= rangeEnd
            nothingIf fun a b =
                if fun a b
                  then Nothing
                  else Just False

applyRules :: (MonadIO m, Failable m) => Rule -> RoutingT m ()
applyRules (_, RouteVia idb) = do
  result <- runMaybeT $ do
             intf <- getIntfM idb
             lift $ routeVia intf
  when (isNothing result) $ do
    infoM "Routing.Outgoing" $
              "Dropping policy routed traffic out inactive or missing interface: " ++ show idb
    setResult Drop
      where routeVia outputIntf@Interface{..}
                | outputIntf <?> can TxData = do
                    State {..} <- get
                    debugM "Routing.Outgoing" $ "Routing packet " ++ (show $ getPktId _packet)
                           ++ " -> " ++ show _intfName
                    liftIO $ sendPacket _packet _inputIntf outputIntf
                    setResult Routed
            routeVia Interface{..} | otherwise = do
              debugM "Routing.Outgoing" $
                         "Discarding unroutable packet to receive only interface " ++ show _intfName
              setResult Drop
applyRules (priority, Loadbalance routeMap) = do
  pkt  <- use packet
  flow <- getPacketFlow
  let index    = hash flow
      dstIP    = getDstIP pkt
  result <- runMaybeT $ do
    routingMap <- MaybeT $ deRef routeMap
    routeVia <- MaybeT . pure $ lookupRoute dstIP routingMap
    let numRoutes     = V.length routeVia
        selectedRoute = index `mod` numRoutes
        outputIntf    = routeVia ! selectedRoute
    return (priority, RouteVia outputIntf)
  maybe missingMap applyRules result
      where missingMap = do
              infoM "Routing.Outgoing" $ "Missing route map " ++ (unpack $ getRefName routeMap)
              setResult Drop
            lookupRoute (IPv4 ip) RouteMap{..} = RT.lookup (makeAddrRange ip 32)  routeMapIPv4Table
            lookupRoute (IPv6 ip) RouteMap{..} = RT.lookup (makeAddrRange ip 128) routeMapIPv6Table
applyRules (priority, ApplyFilter filterRef) = do
  State{..} <- get
  conn <- maybe missingConnection return _connection
  result <- runMaybeT $ do
    filtr@Filter {..}   <- MaybeT $ deRef filterRef
    (result, conn') <- analyze conn filtr _packet
    connection .= Just conn'
    lift $
      case result of
        Undecisive ->
          applyRules (priority, _filterDefault)
        Incomplete ->
          setResult Bypass -- allow packet to flow for now until more data is gathered
        Finished -> do
          action <- evalFilter filtr conn' _packet
          applyRules (priority, action)
        Failed ->
          return ()
  maybe missingFilter return result
      where missingFilter =
              infoM "Routing.Outgoing" $ "Missing filter " ++ (unpack $ getRefName filterRef)
            missingConnection = do
              flow <- getPacketFlow
              infoM "Routing.Outgoing" $ "Missing connection " ++ show flow
              failure $ InternalError "Missing connection"
applyRules (_, Continue) = setResult Bypass
applyRules (_, Discard)  = setResult Drop

getPacketFlow :: (Monad m) => RoutingT m Flow
getPacketFlow = do
  pkt    <- use packet
  pFlow  <- use packetFlow
  vrfID  <- use $ inputIntf . intfVRFId
  let calcFlow = flowFor vrfID pkt
      flow     = fromMaybe calcFlow pFlow
  packetFlow .= Just flow
  return flow

getDirection :: (Monad m, Failable m) => Connection -> RoutingT m Direction
getDirection Connection {..} = do
  pkt           <- use packet
  dir           <- use direction
  connectionSrc <- maybe (failure $ InternalError "Improperly setup connection") return _source
  let figureOutDirection = if getSrcIP pkt == connectionSrc
                             then Upstream
                             else Downstream
      flowDirection = fromMaybe figureOutDirection dir
  direction .= Just flowDirection
  return flowDirection

handleRxPacket :: (MonadIO m, Failable m) => String -> IDB -> ByteString -> m ()
handleRxPacket name idb bytes = do
  result <- withUnsafeL3Packet bytes $ \pkt ->
    runExceptT $ do
      result' <- lift $ routePacket idb pkt
      case result' of
        Abort ->
          failure . Aborted $ "Routing over interface " ++ name ++ " has been aborted"
        Bypass ->
          inject pkt 0
        _ ->
          return ()
  either failure return result
