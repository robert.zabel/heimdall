{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Route Maps
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.RouteMaps where

import Control.Concurrent.Async    (Async, async)
import Control.Monad               (mapM)
import Control.Monad.Except        (runExceptT)
import Control.Monad.Failable      (Failable(..))
import Control.Monad.IO.Class      (MonadIO)
import Control.Monad.Trans.Maybe   (MaybeT(..))
import Data.Attoparsec.ByteString  (parseOnly)
import Data.ByteString.Char8       (ByteString)
import Data.Configurable           (Configurable, deserialize)
import Data.Maybe                  (catMaybes)
import Data.Referable              (Dict, Referable(..))
import Data.Vector                 (Vector)
import Database.Adapter            (getMap)
import Heimdall.Common.ObjectModel (getNameFromKey)
import Heimdall.Interfaces         (getIDB)
import Heimdall.Types
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.ObjectModel        (monitor)
import System.IO.Unsafe            (unsafePerformIO)

import qualified Data.IP.RouteTable as RT
import qualified Data.Vector        as Vector

type RouteMaps = Dict RouteMapName RouteMap

{-# NOINLINE routeMaps #-}
routeMaps :: RouteMaps
routeMaps = unsafePerformIO newDict

getRouteMapRef :: (MonadIO m) => RouteMapName -> m RouteMapRef
getRouteMapRef = getRef routeMaps

getRouteMapM :: (MonadIO m, Failable m) => RouteMapName -> MaybeT m RouteMap
getRouteMapM = MaybeT . getRouteMap

getRouteMap :: (MonadIO m, Failable m) => RouteMapName -> m (Maybe RouteMap)
getRouteMap = (=<<) deRef . getRouteMapRef

runRouteMaps :: (?env::Env) => IO (Async ())
runRouteMaps =
    async $ monitor "RouteMaps" [routeMapKeyPrefix] routeMapChanged routeMapDeleted

routeMapChanged :: (?env::Env) => ByteString -> IO ()
routeMapChanged key = do
  entries  <- getMap db key
  v4Routes <- catMaybes <$> mapM parseRoute entries
  v6Routes <- catMaybes <$> mapM parseRoute entries
  let name = getNameFromKey key
  updateRef routeMaps name RouteMap {
                        routeMapName      = name,
                        routeMapIPv4Table = RT.fromList v4Routes,
                        routeMapIPv6Table = RT.fromList v6Routes }
      where db = getDB ?env

routeMapDeleted :: (?env::Env) => ByteString -> IO ()
routeMapDeleted key = do
  entries <- getMap db key
  case entries of
    [] -> do
      let name = getNameFromKey key
      killRef routeMaps name
    _ ->
      routeMapChanged key
  where db = getDB ?env

parseRoute :: (Configurable a) => (ByteString, ByteString) -> IO (Maybe (a, Vector IDB))
parseRoute (prefixStr, intfList) = do
  result <- runExceptT $ do
             prefix    <- deserialize prefixStr
             intfsStr  <- deserialize intfList
             intfNames <- mapM parse intfsStr
             intfs     <- Vector.fromList <$> mapM getIDB intfNames
             return (prefix, intfs)
  return $ either (const $ Nothing) return result
      where parse = either (failure . InvalidValue) return . parseOnly parseIntfName
