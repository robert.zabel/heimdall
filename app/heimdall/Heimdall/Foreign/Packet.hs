module Heimdall.Foreign.Packet where

import Control.Exception                (bracket)
import Control.Monad.Failable           (Failable(..))
import Control.Monad.IO.Class           (MonadIO, liftIO)
import Data.ByteString                  (ByteString)
import Data.ByteString.Unsafe           (unsafeUseAsCStringLen)
import Foreign.C.Types                  (CChar, CInt(..), CUInt(..))
import Foreign.Ptr                      (Ptr)
import Foreign.Storable                 (peek)

import Heimdall.Exceptions
import Heimdall.Packet

foreign import ccall "l3.h parse_l3" parse_l3 :: CUInt ->
                                                 Ptr CChar ->
                                                 CUInt ->
                                                 Ptr Packet ->
                                                 IO CInt

foreign import ccall "heimdall.h allocate_pkt" allocate_pkt :: IO (Ptr Packet)
foreign import ccall "heimdall.h free_pkt"     free_pkt     :: Ptr Packet -> IO ()

withUnsafeL3Packet :: (MonadIO m, Failable m)
                      => ByteString
                      -> (Packet -> IO a)
                      -> m a
withUnsafeL3Packet bytes action = do
  result <- liftIO $ bracket allocate_pkt free_pkt $ \pktPtr -> do
    unsafeUseAsCStringLen bytes $ \(bytesPtr, len) -> do
      result <- parse_l3 0 bytesPtr (fromIntegral len) pktPtr
      if result >= 0 then do
          pkt <- peek pktPtr
          Right <$> action pkt
      else
          Left <$> return (NetworkIOError $ "Failed to parse L3 packet: error " ++ show result)
  either failure return result
