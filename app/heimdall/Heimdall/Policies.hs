{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Routing Policies
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Policies where

import Prelude hiding (lookup)
import Control.Concurrent.Async    (Async, async)
import Control.Exception           (throw)
import Control.Monad               (mapM)
import Control.Monad.Except        (runExceptT)
import Control.Monad.Failable      (Failable(..))
import Control.Monad.IO.Class      (MonadIO, liftIO)
import Data.ByteString.Char8       (ByteString, stripPrefix, unpack)
import Data.Configurable           (deserialize)
import Data.IORef                  (newIORef)
import Data.List                   (sortOn)
import Data.Maybe                  (fromMaybe)
import Data.Monoid                 ((<>))
import Data.Referable              (Dict, Referable(..))
import Database.Adapter            (getMap)
import Heimdall.ACLs               (getACLRef)
import Heimdall.Environment
import Heimdall.Exceptions
import Heimdall.Interfaces         (getIDB)
import Heimdall.ObjectModel        (monitor)
import Heimdall.RouteMaps          (getRouteMapRef)
import Heimdall.Types
import System.IO.Unsafe            (unsafePerformIO)
import System.Logging       hiding (monitor)
import Text.Read                   (readEither)

import qualified Data.Map                         as Map

type PolicyRefs = Dict ByteString Policy

{-# NOINLINE policies #-}
policies :: PolicyRefs
policies = unsafePerformIO $ newIORef Map.empty

getPolicyRef :: (MonadIO m) => ByteString -> m PolicyRef
getPolicyRef = getRef policies

getPolicyMapEntry :: (MonadIO m) => (ByteString, ByteString) -> m (ACLRef, PolicyRef)
getPolicyMapEntry (aclName, policyName) = do
  aclRef    <- getACLRef aclName
  policyRef <- getPolicyRef policyName
  return (aclRef, policyRef)

updatePolicy :: (MonadIO m, Failable m) => ByteString -> Policy -> m ()
updatePolicy = updateRef policies

deletePolicy :: (MonadIO m) => ByteString -> m ()
deletePolicy = killRef policies

badAccess :: (MonadIO m) => String -> m ()
badAccess = throw . BadAccess

readPolicy :: (?env::Env) => ByteString -> IO Policy
readPolicy name = do
  let db = getDB ?env
  kvs     <- getMap db (policyKeyPrefix <> name)
  actions <- mapM (importRule name) kvs
  return Policy { policyName  = name,
                  policyRules = sortOn fst [ a | Just a <- actions ] }

importRule :: ByteString -> (ByteString, ByteString) -> IO (Maybe Rule)
importRule name (priorityStr, actionStr) = do
  rule <- runExceptT $ do
           priority <- either (failure . InvalidValue) return . readEither $ unpack priorityStr
           action   <- deserialize actionStr >>= instantiateAction
           return (priority, action)
  either badRule (return . Just) rule
      where badRule err = do
                warningM "Policies" $ "Invalid rule " ++ unpack priorityStr
                             ++ " in policy " ++ unpack name ++ ": " ++ unpack actionStr
                             ++ ": " ++ show err
                return Nothing

instantiateAction :: (MonadIO m, Failable m) => Action -> m Action
instantiateAction (RouteVia deadIDBRef) =
  RouteVia <$> getIDB name
    where name = getRefName deadIDBRef
instantiateAction (Loadbalance deadRouteMapRef) =
  Loadbalance <$> getRouteMapRef routeMapName
    where routeMapName = getRefName deadRouteMapRef
instantiateAction action =
  return action

runPolicies :: (?env::Env) => IO (Async())
runPolicies =
    async $ monitor "Policies" [policyKeyPrefix] policyChanged policyDeleted

impossible :: String -> a
impossible = throw . InternalError . (++ "Impossible condition: ")

policyNameFrom :: ByteString -> ByteString
policyNameFrom key = fromMaybe badKey $ stripPrefix policyKeyPrefix key
      where badKey = impossible $ "routing map change on malformed key " ++ unpack key

policyChanged :: (?env::Env, MonadIO m) => ByteString -> m ()
policyChanged key = liftIO $ do
  let name = policyNameFrom key
  policy <- readPolicy name
  updatePolicy name policy

policyDeleted :: (?env::Env, Failable m, MonadIO m) => ByteString -> m ()
policyDeleted key = do
  entries <- getMap db key
  case entries of
    [] -> liftIO . deletePolicy $ policyNameFrom key
    _  -> policyChanged key
  where db = getDB ?env

importPolicyMap :: (?env::Env, MonadIO m, Failable m) => ByteString -> m PolicyMap
importPolicyMap key = do
  let policyMapName = key <> ".policies"
  policyNames <- getMap db policyMapName
  mapM getPolicyMapEntry policyNames
    where db = getDB ?env
