{-# LANGUAGE TemplateHaskell #-}

module Heimdall.Connections where

import Control.Lens          (makeLenses)
import Data.Default          (Default(..))
import Data.IP               (IP)
import Heimdall.Types.Analysis

data Connection = Connection { _source    :: Maybe IP,
                               _analState :: Maybe AnalState
                             }

instance Default Connection where
    def = Connection { _source    = Nothing,
                       _analState = Nothing }

makeLenses ''Connection