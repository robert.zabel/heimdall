module Heimdall.Interface.VIF.Types where

import Data.ByteString          (ByteString)
import Control.Concurrent.MVar  (MVar)
import Heimdall.Types           (IDB)
import Network.Socket           (Socket)

data VIF = VIF { vifId          :: Int,
                 vifDevName     :: Maybe ByteString,
                 vifDevType     :: Maybe ByteString,
                 vifIDB         :: IDB,
                 vifSkVar       :: MVar DualSocket }

newtype DualSocket = DualSocket { unwrapDualSocket :: (Socket, Socket) }
