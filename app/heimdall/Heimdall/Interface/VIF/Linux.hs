{-# LANGUAGE OverloadedStrings #-}
module Heimdall.Interface.VIF.Linux where

import Control.Monad                   (void, when)
import Control.Monad.Failable          (failableIO)
import Control.Monad.Writer            (WriterT, execWriterT, lift, tell)
import Control.Monad.Trans.Maybe       (MaybeT(..), runMaybeT)
import Control.Monad.IO.Class          (MonadIO, liftIO)
import Data.ByteString.Char8           (unpack)
import Data.Maybe                      (fromMaybe)
import Data.Print                      (toString)
import Heimdall.Types
import Heimdall.Interfaces             (getIntf)
import Heimdall.Interface.VIF.Types
import System.Exit                     (ExitCode(..))
import System.Logging
import System.Process                  (callCommand, runCommand, waitForProcess)
import System.Timeout                  (timeout)

getTable :: String
getTable = "nat"

getChain :: String
getChain = "POSTROUTING"

isMasquerading :: (?vif::VIF, MonadIO m) => m Bool
isMasquerading = do
  let VIF {..} = ?vif
  mIntf <- liftIO $ getIntf vifIDB
  return $ fromMaybe False $ mIntf >>= return . _intfMasquerading

getConnTag :: (?vif::VIF, MonadIO m) => m (Maybe ConnectionTag)
getConnTag = do
  let VIF {..} = ?vif
  mIntf <- liftIO $ getIntf vifIDB
  return $ _intfConnTag =<< mIntf

getDevName :: (?vif::VIF, Monad m) => m (Maybe String)
getDevName = return $ vifDevName ?vif >>= return . unpack

getDevType :: (?vif::VIF, Monad m) => m (Maybe String)
getDevType = return $ vifDevType ?vif >>= return . unpack

(<+>) :: (Monad m) => WriterT String m () -> String -> WriterT String m ()
(<+>) actions str = actions >> tell (' ':str)

baseCmd :: (Monad m) => String -> WriterT String m ()
baseCmd oper =
  tell "sudo iptables -t" <+> getTable <+> oper <+> getChain

masqueradingRule :: (?vif::VIF) => String -> WriterT String IO ()
masqueradingRule oper = do
    baseCmd oper
    getConnTag >>= perhaps "-m mark --mark "
    getDevName >>= perhaps "-o "
    tell " -j MASQUERADE"
        where perhaps _ Nothing    = return ()
              perhaps arg (Just x) = tell $ ' ':arg ++ ' ':toString x

platformVIFStart :: VIF -> IO ()
platformVIFStart vif@VIF {..} = do
  let ?vif = vif
  platformVIFStop vif -- für alle Fälle
  masquerading <- isMasquerading
  when masquerading $ iptables $ masqueradingRule "-A"
  void . runMaybeT $ do
    devName <- MaybeT getDevName
    devType <- fromMaybe "dummy" <$> lift getDevType
    void . lift . callCommand $ "sudo ip link add " ++ devName ++ " type " ++ devType
    lift . callCommand $ "sudo ip link set " ++ devName ++ " up"

platformVIFStop :: VIF -> IO ()
platformVIFStop vif = do
  let ?vif = vif
  masquerading <- isMasquerading
  when masquerading $ iptables $ masqueradingRule "-D"
  void . runMaybeT $ do
    devName <- MaybeT getDevName
    failableIO . callCommand $ "sudo ip link delete " ++ devName

iptables :: WriterT String IO () -> IO ()
iptables actions = do
  cmd <- execWriterT actions
  debugM "VIF.IPTables" $ "Issuing " ++ cmd
  result <- timeout 2000000 $ runCommand cmd >>= waitForProcess
  when (result /= Just ExitSuccess) $
       infoM "VIF.IPTables" $ "Command " ++ cmd ++ " failed: " ++ show result
