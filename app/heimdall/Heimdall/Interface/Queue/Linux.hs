module Heimdall.Interface.Queue.Linux where

import Control.Exception               (throw)
import Control.Monad                   (guard, mapM_, void, when)
import Control.Monad.Trans             (lift)
import Control.Monad.Trans.Maybe       (MaybeT(..), runMaybeT)
import Control.Monad.Writer            (Writer, runWriter, tell)
import Data.Print                      (toString)
import Data.Referable                  (deRef)
import Heimdall.Common.ACL             (aceFamily)
import Heimdall.Exceptions
import Heimdall.Interface.Queue.Types
import Heimdall.Packet
import Heimdall.Types
import Network.Socket                  (Family(..))
import System.Exit                     (ExitCode(..))
import System.Logging
import System.Process                  (runCommand, waitForProcess)
import System.Timeout                  (timeout)

qChainPrefix :: String
qChainPrefix = "HEIMDALL_QUEUE_"

targetChain :: (?qNum::String) => String
targetChain = qChainPrefix ++ ?qNum

(<+>) :: Writer String () -> String -> Writer String ()
(<+>) actions str = actions >> tell (' ':str)

baseCmd :: (?table :: String) => String -> String -> Writer String ()
baseCmd oper chain =
  tell "-t" <+> ?table <+> oper <+> chain

qChainRule :: (?table :: String, ?chain::String, ?qNum::String) => String -> Writer String ()
qChainRule oper =
  baseCmd oper ?chain  <+> "-j" <+> targetChain

qChain :: (?table :: String, ?chain::String) => String -> Writer String ()
qChain oper = baseCmd oper ?chain

insertQueueChainRule :: (?table::String, ?qNum::String) => ServiceType -> ACE -> Writer String (Maybe Family)
insertQueueChainRule direction ace@ACE {..} = do
    baseCmd "-A" targetChain
    void . runMaybeT $ do
      proto <- MaybeT . pure $ _aceProtocol
      guard (proto == TCP_Protocol || proto == UDP_Protocol)
      let protoStr = show proto
      lift $ tell " -p" <+> protoStr <+> "-m" <+> protoStr
    perhaps "--dport" _acePorts
    perhaps "-s" _aceSrcRange
    perhaps "-d" _aceDstRange
    perhaps directionOp _aceDevice
    perhaps "-m mark --mark " _aceConnTag
    if _aceAccess == Permit
      then tell " -j NFQUEUE --queue-num" <+> ?qNum
      else tell " -j RETURN"
    return $ aceFamily ace
        where perhaps _ Nothing    = return ()
              perhaps arg (Just x) = tell (' ':arg) <+> toString x
              directionOp = case direction of
                              IngressService    -> "-i"
                              EgressService     -> "-o"
                              ForwardingService -> "-i"

platformQueueStart :: (?queue::Queue) => IO ()
platformQueueStart = do
  let ?table = table
      ?chain = chain
      ?qNum  = show $ qId ?queue
  platformQueueStop mempty -- für alle Fälle
  createQueueChain
  insertQueueChainJump
  insertQueueChainRules
      where (table, chain) = getChainFor $ qServiceType ?queue

platformQueueStop :: (?queue::Queue) => () -> IO ()
platformQueueStop _ = do
  let ?table = table
      ?chain = chain
      ?qNum  = show $ qId ?queue
  deleteQueueChainJump
  deleteQueueChain
      where (table, chain) = getChainFor $ qServiceType ?queue

getChainFor :: ServiceType -> (String, String)
getChainFor IngressService    = ("raw", "PREROUTING")    -- thinking, before it goes thru CONNTRACK
getChainFor EgressService     = ("mangle", "POSTROUTING")
getChainFor ForwardingService = ("mangle", "FORWARD")

deleteQueueChainJump :: (?table::String, ?chain::String, ?qNum::String) => IO ()
deleteQueueChainJump = iptables $ qChainRule "-D" >> return Nothing

insertQueueChainJump :: (?table::String, ?chain::String, ?qNum::String) => IO ()
insertQueueChainJump = iptables $ qChainRule "-A" >> return Nothing

deleteQueueChain :: (?table::String, ?qNum::String) => IO ()
deleteQueueChain = do
  let ?chain = targetChain
  iptables $ qChain "-F" >> return Nothing
  iptables $ qChain "-X" >> return Nothing

createQueueChain :: (?table::String, ?qNum::String) => IO ()
createQueueChain = do
  let ?chain = targetChain
  iptables $ qChain "-N" >> return Nothing

insertQueueChainRules :: (?table::String, ?qNum::String, ?queue::Queue) => IO ()
insertQueueChainRules =
  void. runMaybeT $ do
    aclRef  <- MaybeT . pure $ qACL ?queue
    ACL{..} <- MaybeT $ deRef aclRef
    lift $ mapM_ (iptables . insertQueueChainRule serviceType) $ fmap snd aclRules
        where serviceType = qServiceType ?queue

iptables :: Writer String (Maybe Family) -> IO ()
iptables actions = do
  let (family, cmd) = runWriter actions
  mapM_ runOnTable $ (++ cmd) <$> onTablesFor family
    where runOnTable cmd = do
            debugM "Queues.IPTables" $ "Issuing " ++ cmd
            result <- timeout 2000000 $ runCommand cmd >>= waitForProcess
            when (result /= Just ExitSuccess) $
              infoM "Queues.IPTables" $ "Command " ++ cmd ++ " failed: " ++ show result
          onTablesFor (Just AF_INET)  = ["sudo iptables "]
          onTablesFor (Just AF_INET6) = ["sudo ip6tables "]
          onTablesFor (Just family)   = throw $ UnsupportedAddressFamily family
          onTablesFor Nothing         = onTablesFor (Just AF_INET) ++ onTablesFor (Just AF_INET6)
