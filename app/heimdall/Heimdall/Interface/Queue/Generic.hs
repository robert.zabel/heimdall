module Heimdall.Interface.Queue.Generic where

platformQueueStart :: IO ()
platformQueueStart = return ()

platformQueueStop :: () -> IO ()
platformQueueStop _ = return ()
