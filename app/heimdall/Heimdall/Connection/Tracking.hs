{-# LANGUAGE OverloadedStrings #-}
module Heimdall.Connection.Tracking where


import Control.Concurrent.Async          (Async, async)
import Control.Concurrent.MVar           (MVar, modifyMVar_, modifyMVar, newMVar, readMVar)
import Control.Monad.Except              (runExceptT)
import Control.Monad.Failable            (Failable(..))
import Control.Monad.IO.Class            (MonadIO, liftIO)
import Data.ByteString.Char8             (ByteString, unpack)
import Data.Configurable                 (Configurable)
import Data.Default                      (def)
import Data.Maybe                        (fromMaybe)
import Data.TTLHashTable                 (TTLHashTable)
import Heimdall.Common.ObjectModel       (getNameFromKey)
import Heimdall.Connections              (Connection)
import Heimdall.Environment
import Heimdall.Network.Flow             (Flow)
import Heimdall.ObjectModel              (deserializeVal, monitor)
import Heimdall.Types
import System.IO.Unsafe                  (unsafePerformIO)
import System.Logging             hiding (monitor)

import qualified Data.HashTable.ST.Basic as Basic
import qualified Data.TTLHashTable       as HT

type HashTable = TTLHashTable Basic.HashTable

type ConnTable = HashTable Flow Connection

data Context = Context {
      vrfTableVar :: MVar (HashTable VRFID ConnTable),
      paramsVar   :: MVar Parameters
    }

data Parameters = Parameters { ttl :: Int }

{-# NOINLINE context #-}
context :: Context
context = unsafePerformIO $ do
            let defaultTTL = 60000 -- 60s default connection timeout
            ht <- HT.newWithSettings def { HT.defaultTTL = defaultTTL }
            vrfTableVar <- newMVar ht
            paramsVar   <- newMVar Parameters { ttl = defaultTTL }
            return Context { vrfTableVar = vrfTableVar,
                             paramsVar   = paramsVar }

run :: (?env::Env) => IO (Async ())
run =
  async $ monitor "Connection.Tracking" [connTrackKeyPrefix] configChange configChange

configChange :: (?env::Env) => ByteString -> IO ()
configChange key =
  update name
      where name = getNameFromKey key

reconfigureTableWith :: (HT.Settings -> HT.Settings) -> HashTable k v -> IO ()
reconfigureTableWith mutator ht = do
  settings <- HT.getSettings ht
  HT.reconfigure ht $ mutator settings

update :: (?env::Env) => ByteString -> IO ()
update name@"ttl" =
  updateTableSetting name updateTTL setTTL
      where updateTTL ttl s = s { HT.defaultTTL = ttl }
            setTTL ttl p    = p { ttl = ttl }
update name =
  warningM "Connection.Tracking" $ "Unknown configuration key " ++ unpack name

updateTableSetting :: (?env::Env, Configurable a)
                      => ByteString
                      -> (a -> HT.Settings -> HT.Settings)
                      -> (a -> Parameters -> Parameters)
                      -> IO ()
updateTableSetting name updateSettings updateParams = do
  result <- runExceptT $ do
    val <- deserializeVal connTrackKeyPrefix name
    let updateVal = reconfigureTableWith $ updateSettings val
    liftIO $ reconfigureTables updateVal updateVal
    return val
  flip (either failed) result $ \val -> do
      let Context {..} = context
      modifyMVar_ paramsVar $ return . updateParams val
          where failed e = warningM "Connection.Tracking" $
                             "Invalid configuration " ++ unpack name ++ ": " ++ show e

reconfigureTables :: (?env::Env)
                   => (HashTable VRFID ConnTable -> IO ())
                   -> (ConnTable -> IO ())
                   -> IO ()
reconfigureTables mutateVT mutateCT = do
  let Context {..} = context
  modifyMVar_ vrfTableVar $ \vrfTable -> do
    mutateVT vrfTable
    HT.mapM_ (mutateCT . snd) vrfTable
    return vrfTable

createConnectionTable :: IO ConnTable
createConnectionTable = do
  let Context {..} = context
  Parameters {..} <- readMVar paramsVar
  HT.newWithSettings def { HT.defaultTTL = ttl }

updateConnection :: (MonadIO m, Failable m)
                    => VRFID
                    -> Flow
                    -> (Connection -> Connection)
                    -> m Connection
updateConnection vrfID flow mutate = do
  let Context {..} = context
  result <- liftIO . modifyMVar vrfTableVar $ \vrfTable -> do
             mCT    <- HT.find vrfTable vrfID
             ct     <- maybe createConnectionTable return mCT
             result <- runExceptT $ HT.mutate ct flow connection
             return (vrfTable, result)
  either failure return result
      where connection mConn =
                let conn = mutate $ fromMaybe def mConn
                in (return conn, conn)
