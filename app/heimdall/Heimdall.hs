{-# LANGUAGE OverloadedStrings #-}
{- |
Description: H.E.I.M.D.A.L.L - Highly Effective Intervention Manipulation and Data Analysis at the
                               Link Layer
Copyright: (c) Erick Gonzalez, 2018
License: LGPL (See LICENSE)
Maintainer: erick@codemonkeylabs.com

Passively monitors data flows according to configured rules, triggering intervention
actions to allow other network components to intercept or reroute a connection as needed.

-}
module Heimdall where

import Control.Monad.Trans           (lift)
import Control.Monad.Trans.Writer    (WriterT(..), execWriterT, tell)
import Control.Concurrent.Async      (Async, wait)
import Data.Maybe                    (fromMaybe)
import Database.Adapter              (new)
import Heimdall.ACLs                 (runACLs)
import Heimdall.Environment          (Env(..))
import Heimdall.Policies             (runPolicies)
import Heimdall.Interface.Queues     (runQueues)
import Heimdall.RouteMaps            (runRouteMaps)
import Heimdall.Interface.Tunnels    (runTunnels)
import Heimdall.Interface.VIFs       (runVIFs)
import System.Environment            (getArgs)

import qualified System.Logging         as L

-- | Entry point for the Heimdall service
runService :: IO ()
runService = do
  db      <- new
  args    <- parseOptions <$> getArgs
  let ?env = Env { getDB   = db,
                   getNode = fromMaybe missingNodeName $ lookup "--node" args >>= id }
  monitors <- execWriterT $ do
               spawn $ L.monitor db
               spawn runPolicies
               spawn runRouteMaps
               spawn runQueues
               spawn runTunnels
               spawn runVIFs
               spawn runACLs
  mapM_ wait monitors
      where missingNodeName = error $ "Missing local node name. Specify it via the --node command "
                                ++ "argument"

spawn :: IO (Async ()) -> WriterT [Async ()] IO ()
spawn fn = lift fn >>= tell . pure

isOption :: String -> Bool
isOption ('-':_) = True
isOption _       = False

parseOptions :: [String] -> [(String, Maybe String)]
parseOptions args =
    parseOptions' args []

parseOptions' :: [String] -> [(String, Maybe String)] -> [(String, Maybe String)]
parseOptions' []     acc = acc
parseOptions' (x:rest@(x':xs)) acc
    | isOption x = if isOption x'
                     then parseOptions' rest ((x, Nothing):acc)
                     else parseOptions' xs ((x, Just x'):acc)
    | otherwise  = error $ "unexpected argument: " ++ x
parseOptions' [x] acc
    | isOption x = (x, Nothing):acc
    | otherwise  = error $ "unexpected trailing argument: " ++ x
