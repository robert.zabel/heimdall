H.E.I.M.D.A.L.L - Highly Effective Intervention Manipulation and Data Analysis at the Link Layer
================================================================================================
![logo](https://images-na.ssl-images-amazon.com/images/I/511eVlnuuuL.jpg)

In essence, HEIMDALL is a _user space data traffic routing and manipulation application_ or
in short _a router_. Packets are routed and analyzed according to programmable policies, making
things like loadbalancing source routed traffic possible. Furthermore, configuration and
control of HEIMDALL can be done via a provided cli tool (named _Hofund_) or by external
applications by manipulating configuration keys directly into its DB backend (currently [Redis](https://redis.io)
)

The concept of HEIMDALL arised due to a need to provide traffic distribution and layer 2
analysis in a _large scale clustered environment_. So while not strictly necessary, it is
expected that HEIMDALL be deployed in a multi node setup. The work and components necessary to
build a cluster are of course up to the specific application and have nothing to do with 
HEIMDALL per se, so it provides in this regard no facilities for replication or otherwise cluster
operation etc, beyond the fact that there _is_ a concept of a node used in some places. What
a node actually is or how it runs or how it synchronizes with other nodes is as already mentioned, 
an application specific concern and outside the scope of HEIMDALL. For example at Secucloud, we used
an internally developed custom propietary DB replication component to make Redis behave similar to a 
Amazon Dynamo DB. For another application you could simply use clustered Redis or develop your own etc.

## Getting Started

### Building HEIMDALL

1. Install [Stack](http://haskellstack.org)

```
curl -sSL https://get.haskellstack.org/ | sh
```

2. Install [Nix](https://nixos.org/nix/)

```
curl https://nixos.org/nix/install | sh
```

3. Clone this repository

```
git clone git@gitlab.com:codemonkeylabs/heimdall
```

4. Build the project

```
stack build
```

### Starting HEIMDALL
1. Start [Redis](https://redis.io)

```
stack exec redis-server
```
2. Start HEIMDALL

```
stack exec heimdall -- --node <name>
``` 
(replace of course <name> with a unique node name)

### Using HEIMDALL

There are essentially two ways to configure HEIMDALL: using the provided cli (_Hofund_) or 
by direct interaction with Redis keys in the DB (either via _redis-cli_ or by using any of the
myriad language APIs available for Redis for direct control from your own program)

#### Hofund

Manual configuration of HEIMDALL via cli can be done with help of Hofund. Start the cli:

```
stack exec hofund
```

Those familiar with Cisco IOS style hierarchical cli's will be right at home. 
A command reference for Hofund can be found in the [Wiki page](https://gitlab.com/codemonkeylabs/heimdall/wikis/Usage-and-Configuration#hofund-cli)

Similarly there is a Redis configuration key reference in the respective [Wiki page](https://gitlab.com/codemonkeylabs/heimdall/wikis/Configuration-Key-Reference)

## LICENSE
HEIMDALL is distributed under the LGPL v2.1 license. It makes use of some libraries from the
Linux Netfilter project which are distributed under the terms of the GPL as well. 


