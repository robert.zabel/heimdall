{-# LANGUAGE OverloadedStrings #-}
{- |
Description: Heimdall Name-Referable class
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Data.Referable where

import Control.Concurrent        (ThreadId, myThreadId)
import Control.Exception         (Exception(..), SomeException, throw, throwTo)
import Control.Monad             (void)
import Control.Monad.Failable    (Failable(..))
import Control.Monad.IO.Class    (MonadIO, liftIO)
import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
import Data.Bifunctor            (first)
import Data.IORef                (IORef, atomicModifyIORef', newIORef, readIORef)
import Data.Map                  (Map)
import Data.Print                (Print(..))
import Data.Set                  (Set)
import Data.Typeable             (Typeable)

import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Heimdall.Exceptions as HE

type TombStone          = ()
type Box a              = Either TombStone (Maybe a, Watches)
type BoxRef a           = IORef (Box a)
newtype VolatileRef k a = VolatileRef { unwrapVolatileRef :: (k, BoxRef a) }
type Watches            = Set Watch
type Watch              = ThreadId

type Dict k v  = IORef (Map k (VolatileRef k v))

instance (Print k) => Show (VolatileRef k a) where
    showsPrec _ (VolatileRef (key, _)) = (('#':toString key) ++)

tombStone :: Box a
tombStone = Left ()

getBoxContents :: Box a -> Either TombStone (Maybe a)
getBoxContents = fmap fst

data ReferableException k = RefChange k
                          | RefDelete k
                          | RefKilled k
                            deriving (Show, Typeable)

instance (Show k, Typeable k) => Exception (ReferableException k)

class Referable a where

    newDict :: (Ord k, MonadIO m) => m (Dict k a)
    newDict = liftIO $ newIORef Map.empty

    deRef :: (MonadIO m, Failable m, Print k) => VolatileRef k a -> m (Maybe a)
    deRef (VolatileRef bRef@(name, _)) = do
        result <- assertOp . liftIO . readIORef . snd $ bRef
        either failure return result
            where assertOp = fmap (first (const msg) . getBoxContents)
                  msg      = badRef "deRef" name

    getRef :: (Ord k, MonadIO m) => Dict k a -> k -> m (VolatileRef k a)
    getRef dict name = liftIO $ do
      mVRef <- readIORef dict >>= return . Map.lookup name
      maybe newVRef return mVRef
          where newVRef = do
                   bRef <- newIORef $ Right (Nothing, Set.empty)
                   let vRef = VolatileRef (name, bRef)
                   atomicModifyIORef' dict $ \m ->
                       case Map.lookup name m of
                         Nothing ->
                             (Map.insert name vRef m, vRef)
                         Just vRef' ->
                             (m, vRef')

    updateRef :: (Print k, Show k, Typeable k, Ord k, MonadIO m, Failable m)
                 => Dict k a
                 -> k
                 -> a
                 -> m ()
    updateRef dict name value = do
      VolatileRef (_, bRef) <- getRef dict name
      watches <- liftIO $ atomicModifyIORef' bRef updateRef'
      flip (either failure) watches $ notify (RefChange name)
          where updateRef' r@(Left ())          = (r, failure $ badRef "updateRef" name)
                updateRef' (Right (_, watches)) = (Right (Just value, mempty), return watches)

    delRef :: (Print k, Show k, Typeable k, Ord k, MonadIO m, Failable m) => Dict k a -> k -> m ()
    delRef dict name = do
      VolatileRef (_, bRef) <- getRef dict name
      watches <- liftIO $ atomicModifyIORef' bRef delRef'
      flip (either failure) watches $ notify (RefDelete name)
          where delRef' r@(Left ())          = (r, failure $ badRef "delRef" name)
                delRef' (Right (_, watches)) = (Right (Nothing, mempty), return watches)

    modifyRef :: (Print k, Ord k, Show k, Typeable k, MonadIO m, Failable m)
                 => Dict k a
                 -> k
                 -> (a -> a)
                 -> m ()
    modifyRef dict name fn = do
      VolatileRef (_, bRef) <- getRef dict name
      watches <- liftIO $ atomicModifyIORef' bRef modifyRef'
      flip (either failure) watches $ notify (RefChange name)
          where modifyRef' r@(Left ())          = (r, failure $ badRef "modifyRef" name)
                modifyRef' (Right (v, watches)) = (Right (v >>= Just . fn, mempty), return watches)

    killRef :: (Print k, Ord k, Show k, Typeable k, MonadIO m) => Dict k a -> k -> m ()
    killRef dict name = void . runMaybeT $ do
      VolatileRef (_, bRef) <- MaybeT . liftIO $ atomicModifyIORef' dict $ \m ->
                                (Map.delete name m, Map.lookup name m)
      watches <- MaybeT . liftIO $ atomicModifyIORef' bRef killRef'
      notify (RefKilled name) watches
          where killRef' (Left _)             = (tombStone, Nothing)
                killRef' (Right (_, watches)) = (tombStone, Just watches)

    watchRef :: (Print k, MonadIO m, Failable m) => VolatileRef k a -> m ()
    watchRef (VolatileRef (name, bRef)) = do
      result <- liftIO $ do
        myself <- myThreadId
        atomicModifyIORef' bRef $ watchRef' myself
      either failure return result
          where watchRef' _ r@(Left ())           = (r, failure $ badRef "watchRef" name)
                watchRef' me (Right (v, watches)) = (Right (v, Set.insert me watches), return ())

    deadRef :: (Print k) => k -> VolatileRef k a
    deadRef name = VolatileRef (name, throw $ badRef "deadRef" name)

    getRefName :: VolatileRef k a -> k
    getRefName = fst . unwrapVolatileRef

badRef :: (Print k) => String -> k -> SomeException
badRef loc name = toException . HE.BadAccess $ loc ++ " <dead> #" ++ toString name

notify :: (MonadIO m, Show k, Typeable k) => ReferableException k -> Watches -> m ()
notify ev  = liftIO . mapM_ (flip throwTo $ ev)
