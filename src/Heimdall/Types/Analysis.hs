{-# LANGUAGE GADTs             #-}
{-# LANGUAGE TemplateHaskell   #-}

module Heimdall.Types.Analysis where

import Control.Lens                  (makeLenses)
import Control.Monad.State.Strict    (StateT)
import Data.Default                  (Default, def)
import Data.ByteString               (ByteString)
import Heimdall.Types.Policy         (Action, PolicyRef)
import Heimdall.Types.Filter         (Filter_)
import Heimdall.Packet               (Packet)

import qualified Data.Attoparsec.ByteString.Char8 as A

data Status = Undecisive
            | Incomplete
            | Finished
            | Failed

data ApplicationProtocol = DummyProtocol
                         | HTTP
                         | TLS

class IsAProtocol p

data HTTPMethod = GET | PUT | POST | DELETE | HEAD | TRACE | CONNECT | OPTIONS | PATCH

data HTTPVersion = HTTP1_0
                 | HTTP1_1
                 | HTTP2_0
                 | HTTP3_0

data AppMetadata a where
    DummyData :: AppMetadata a
    HTTPInfo  :: { method  :: Maybe HTTPMethod,
                   uri     :: Maybe ByteString,
                   version :: Maybe HTTPVersion,
                   headers :: [(ByteString, ByteString)] } -> AppMetadata a
    TLSInfo   :: { sni    :: Maybe ByteString } -> AppMetadata a

data L57Analysis = L57Analysis {
                        _protocol    :: Maybe ApplicationProtocol,
                        _appMetadata :: AppMetadata ApplicationProtocol,
                        _continue    :: Maybe (ByteString -> A.Result L57Analysis)
                   }

instance Default L57Analysis where
    def = L57Analysis {
              _protocol    = Nothing,
              _appMetadata = DummyData,
              _continue    = Nothing
          }

data AnalState = AnalState {
                        _analFilter :: Filter_ PolicyRef Action,
                        _analPacket :: Packet
                 }

type AnalMonad = StateT AnalState A.Parser

makeLenses ''AnalState
makeLenses ''L57Analysis
