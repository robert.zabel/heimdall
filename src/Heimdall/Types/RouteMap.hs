{-# LANGUAGE OverloadedStrings #-}

module Heimdall.Types.RouteMap where

import Data.ByteString               (ByteString)
import Data.IP                       (IPv4, IPv6)
import Data.IP.RouteTable            (IPRTable)
import Data.Referable                (Referable, VolatileRef)
import Data.Vector                   (Vector)

type RouteMapName = ByteString

type RouteMapTable_ idb a = IPRTable a (Vector idb)

data RouteMap_ idb = RouteMap { routeMapName      :: RouteMapName,
                                routeMapIPv4Table :: RouteMapTable_ idb IPv4,
                                routeMapIPv6Table :: RouteMapTable_ idb IPv6 }

instance Referable (RouteMap_ idb)

type RouteMapRef_ idb = VolatileRef RouteMapName (RouteMap_ idb)

routeMapKeyPrefix :: ByteString
routeMapKeyPrefix = "config.heimdall.route-map."
