{-# LANGUAGE OverloadedStrings #-}

module Heimdall.Types.Policy where

import Data.ByteString.Char8         (ByteString, pack)
import Data.Char                     (isSpace)
import Data.Configurable             (Configurable(..))
import Data.Default                  (Default, def)
import Data.Referable                (Referable, VolatileRef, getRefName, deadRef)
import Control.Applicative           ((<|>))
import Control.Monad                 (void)
import Control.Monad.Failable        (failure)
import Heimdall.Exceptions
import Heimdall.Types.ACL
import Heimdall.Types.Interface
import Heimdall.Types.Filter
import Heimdall.Types.RouteMap

import qualified Data.Attoparsec.ByteString.Char8 as A

data Action = RouteVia (IDB_ PolicyMap)
            | Loadbalance (RouteMapRef_ (IDB_ PolicyMap))
            | ApplyFilter (FilterRef_ PolicyRef Action)
            | Continue
            | Discard
            deriving Show

type Rule = (Int, Action)

data Policy = Policy { policyName  :: PolicyName,
                       policyRules :: [Rule] }

type PolicyName = ByteString

instance Referable Policy

type PolicyRef = VolatileRef PolicyName Policy

type PolicyMap = [(ACLRef, PolicyRef)]

policyKeyPrefix :: ByteString
policyKeyPrefix = "config.heimdall.policy."

instance Configurable Action where
    serialize (RouteVia idb)            = return $ "route-via " <> (pack . show $ getRefName idb)
    serialize (Loadbalance routeMapRef) = return $ "loadbalance " <> getRefName routeMapRef
    serialize (ApplyFilter filterRef)   = return $ "apply-filter " <> getRefName filterRef
    serialize Discard                   = return $ "discard"
    serialize Continue                  = return $ "continue"
    deserialize = either (failure . InvalidValue) return . A.parseOnly actionParser
        where actionParser = routeViaIntf <|>
                             loadbalanceRouteMap <|>
                             applyFilter <|>
                             discard <|>
                             continue
              discard  = A.string "discard"  >> A.endOfInput >> return Discard
              continue = A.string "continue" >> A.endOfInput >> return Continue
              routeViaIntf = do
                void $ A.string "route-via"
                A.skipWhile isSpace
                RouteVia . deadRef <$> parseIntfName
              loadbalanceRouteMap = do
                void $ A.string "loadbalance"
                A.skipWhile isSpace
                Loadbalance . deadRef <$> A.takeWhile1 (not . isSpace)
              applyFilter = do
                void $ A.string "apply-filter"
                A.skipWhile isSpace
                ApplyFilter . deadRef <$> A.takeWhile1 (not . isSpace)

data Result = Routed
            | Drop
            | Bypass
            | Abort
            | None
              deriving Show

instance Default Result where
    def = None
