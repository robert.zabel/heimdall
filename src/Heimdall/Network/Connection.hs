module Heimdall.Network.Connection (NetworkConnection, create, start, send, recv, recvExactly, terminate) where

import Control.Concurrent              (threadDelay)
import Control.Concurrent.Async        (AsyncCancelled, async, cancel)
import Control.Concurrent.MVar         (newEmptyMVar, putMVar, tryTakeMVar, withMVar)
import Control.Exception               (Handler(..),
                                        SomeException,
                                        bracket_,
                                        catches,
                                        throw)
import Control.Monad                   (void)
import Control.Monad.Failable          (Failable, failableIO)
import Control.Monad.IO.Class          (MonadIO, liftIO)
import Control.Monad.Fix               (fix)
import Control.Monad.Trans.Maybe       (MaybeT(..), runMaybeT)
import Data.ByteString                 (ByteString)
import Heimdall.Types.Network
import Network.Socket                  (HostName, PortNumber)
import qualified Network.Connection    as NC
import System.Mem.Weak                 (mkWeakPtr)

create :: (MonadIO m, Failable m) => HostName -> PortNumber -> Settings -> m NetworkConnection
create host port settings@Settings{..} =
  failableIO $ do
    context <- NC.initConnectionContext
    connVar <- newEmptyMVar
    let connection = NetworkConnection  { _networkConn  = connVar,
                                  _connContext  = context,
                                  _controlProc  = Nothing,
                                  _connSettings = settings,
                                  _connHostName = host,
                                  _connPortNum  = port }
    void . mkWeakPtr connection $ Just $ terminate connection
    let ?connection = connection
    ctrlProcess <- if autoConnect
                    then fmap return . async $ fix controlProcess
                    else return Nothing
    return connection { _controlProc = ctrlProcess }

start :: (MonadIO m, Failable m) => NetworkConnection  -> m ()
start connection@NetworkConnection {..} = do
  close connection
  let tlsSettings = if useTLS _connSettings
                      then return $ NC.TLSSettingsSimple False False True
                      else Nothing
      params      = NC.ConnectionParams { NC.connectionHostname  = _connHostName,
                                          NC.connectionPort      = _connPortNum,
                                          NC.connectionUseSecure = tlsSettings,
                                          NC.connectionUseSocks  = Nothing }
  failableIO $ do
    netConn <- NC.connectTo _connContext params
    putMVar _networkConn netConn

terminate :: (MonadIO m, Failable m) => NetworkConnection  -> m ()
terminate connection@NetworkConnection {..} = do
  void . runMaybeT $ do
    ctrlProc <- MaybeT . return $ _controlProc
    liftIO $ cancel ctrlProc
  close connection

close :: (MonadIO m, Failable m) => NetworkConnection  -> m ()
close NetworkConnection {..} =
  void . failableIO . runMaybeT $ do
    conn <- MaybeT $ tryTakeMVar _networkConn
    liftIO $ NC.connectionClose conn

controlProcess :: (?connection::NetworkConnection ) => IO () -> IO ()
controlProcess loop = do
  handleFailure $ bracket_ (start ?connection) (close ?connection) $ threadDelay maxBound
  loop
      where handleFailure action = action `catches` [Handler uponAsyncCancelled,
                                                     Handler uponSomeException]
            uponAsyncCancelled (e::AsyncCancelled) = throw e
            uponSomeException  (_::SomeException)  = return ()

send :: (MonadIO m, Failable m) => ByteString -> NetworkConnection  -> m ()
send bytes NetworkConnection {..} =
  failableIO $ withMVar _networkConn (`NC.connectionPut` bytes)

recvExactly :: (MonadIO m, Failable m) => Int -> NetworkConnection  -> m ByteString
recvExactly n NetworkConnection  {..} =
  failableIO $ withMVar _networkConn (`NC.connectionGetExact` n)

recv :: (MonadIO m, Failable m) => NetworkConnection  -> m ByteString
recv NetworkConnection {..} =
  failableIO $ withMVar _networkConn NC.connectionGetChunk

