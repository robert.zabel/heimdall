{-# LANGUAGE OverloadedStrings #-}

{- |
Description: Heimdall common base types
License: LGPL
Maintainer: erick@codemonkeylabs.de
Copyright: (c) Erick Gonzalez, 2018
           This library is free software; you can redistribute it and/or
           modify it under the terms of the GNU Lesser General Public
           License as published by the Free Software Foundation; either
           version 2.1 of the License, or (at your option) any later version.

           This library is distributed in the hope that it will be useful,
           but WITHOUT ANY WARRANTY; without even the implied warranty of
           MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
           Lesser General Public License for more details.

           You should have received a copy of the GNU Lesser General Public
           License along with this library; if not, write to the Free Software
           Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
-}
module Heimdall.Types (module Heimdall.Types.ACL,
                       module Heimdall.Types.Analysis,
                       module Heimdall.Types.Connection,
                       module Heimdall.Types.Filter,
                       module Heimdall.Types.Interface,
                       module Heimdall.Types.Network,
                       module Heimdall.Types.Policy,
                       module Heimdall.Types.RouteMap,
                       Filter,
                       Forwarder,
                       ICB,
                       IDB,
                       Interface,
                       RouteMap,
                       RouteMapRef
                       ) where

import Heimdall.Types.ACL
import Heimdall.Types.Analysis
import Heimdall.Types.Connection
import Heimdall.Types.Filter
import Heimdall.Types.Interface
import Heimdall.Types.Network
import Heimdall.Types.Policy
import Heimdall.Types.RouteMap

type ICB       = ICB_ PolicyMap
type IDB       = IDB_ PolicyMap
type Interface = Interface_ PolicyMap

type RouteMap    = RouteMap_ IDB
type RouteMapRef = RouteMapRef_ IDB

type Filter = Filter_ PolicyRef Action

type Forwarder = Forwarder_ PolicyMap